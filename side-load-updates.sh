#!/bin/bash

echo -e "\n\t\e[1m\e[31mSideloading updates\n\e[0m"
docker cp /media/usb/upboard-updates/side-load/server/InertialLabsInsLogger.js  webapp:/usr/src/app/server/
docker cp /media/usb/upboard-updates/side-load/server/config/devices.json  webapp:/usr/src/app/server/config/
docker exec -it webapp dpkg -i /data/upboard-updates/side-load/hesai-monitor_1.0.2_amd64.deb
sleep 1

docker restart webapp
echo -e "\n\tDONE\n"

