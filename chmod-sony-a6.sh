#!/bin/bash

echo -e "\n\tSetting sony-a6-trigger executable. . .\n"
docker exec -it webapp chmod a+x /usr/bin/sony-a6-trigger

echo -e "\n\tsony-a6-trigger permissions: `docker exec -it webapp ls -la a+x /usr/bin/sony-a6-trigger | awk '{print $0}' | awk '{print $1}'`\n"
