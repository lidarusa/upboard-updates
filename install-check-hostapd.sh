#!/bin/bash

echo -e "\n\tCopying check-hostapd. . .\n"
sudo cp /media/usb/upboard-updates/check-hostapd /home/lumberjack/

echo -e "\n\tSetting a+x check-hostapd permissions. . .\n"
sudo chmod a+x /home/lumberjack/check-hostapd

echo -e "\n\tChecking rc.local for check-hostapd. . .\n"
cat /etc/rc.local | grep check-hostapd

if [ $? -eq 0 ]; then

	echo -e "\n\tcheck-hostapd already executed in rc.local.\n"
else
	echo -e "\n\tAdding check-hostapd to rc.local. . .\n"
	
	sudo sed -i 's/exit 0/\/home\/lumberjack\/check-hostapd \&/' /etc/rc.local
	echo -e "\nexit 0" >> /etc/rc.local
	
	echo -e "\n\tDONE\n"
fi

sudo chmod a+x /etc/rc.local
