#!/bin/bash

echo -e "\n\tCopying udev persistent-usb rules. . .\n"
sudo cp /media/usb/upboard-updates/persistent-usb.rules /etc/udev/rules.d/

echo -e "\n\tCopying udev inertial-labs-4dot rules. . .\n"
sudo cp /media/usb/upboard-updates/93-inertial-labs-4dot.rules /etc/udev/rules.d/

echo -e "\n\tudevadm control --reload-rules. . .\n"
sudo udevadm control --reload-rules

echo -e "\n\tDONE\n"


