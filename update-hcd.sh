#!/bin/bash

echo -e "\n\tCurrent host-command-daemon `sudo dpkg -s host-command-daemon | grep Version:`\n"

sudo dpkg -s host-command-daemon | grep Version: | grep 1.3.0

if [ $? -eq 0 ]; then

	echo -e "\n\thost-command-daemon is up to date.\n"

else

	echo -e "\n\tUpdating host-command-daemon. . .\n" 
	ls /media/usb/upboard-updates/host-command-daemon_1.3.0_amd64.deb 2>/dev/null

	if [ $? -eq 0 ]; then
		sudo dpkg -i /media/usb/upboard-updates/host-command-daemon_1.3.0_amd64.deb
		echo -e "\n\tSuccessfully updated host-command daemon to `sudo dpkg -s host-command-daemon | grep Version:`\n"
	else
		echo -e "\n\tCOULD NOT FIND host-command-daemon deb file on thumbdrive! Are you sure its there?\n"
	fi
fi
