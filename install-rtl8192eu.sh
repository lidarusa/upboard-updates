#!/bin/bash

if [ -e /lib/modules/4.4.0-2-upboard/updates/dkms/8192eu.ko ]; then
echo -e "rtl8192eu driver already built and installed."
else
#git clone https://github.com/Mange/rtl8192eu-linux-driver
echo -e "\nPWD: $(pwd)"

if [[ ! -d /home/lumberjack/rtl8192eu-linux-driver ]]; then
cp -r /media/usb/upboard-updates/rtl8192eu-linux-driver /home/lumberjack/
fi

cd /home/lumberjack/rtl8192eu-linux-driver
dkms add .
dkms install rtl8192eu/1.0

if [[ -f "/etc/modprobe.d/rtl8xxxu.conf" ]]; then 
if [[ ! -f "/etc/modprobe.d/rtl8xxxu.conf.BAK" ]]; then
cp -n /etc/modprobe.d/rtl8xxxu.conf{,.BAK}; 
fi
fi
echo "blacklist rtl8xxxu" | sudo tee /etc/modprobe.d/rtl8xxxu.conf


if [[ ! -f "/etc/modules.BAK" ]]; then 
cp -n /etc/modules{,.BAK}
fi
echo -e "\n8192eu\n\nloop" | sudo tee -a /etc/modules

if [ -f "/etc/modprobe.d/8192eu.conf" ]; then 
if [[ ! -f "/etc/modprobe.d/8192eu.conf.BAK" ]]; then
cp -n /etc/modprobe.d/8192eu.conf{,.BAK};
fi
fi
echo "options 8192eu rtw_power_mgnt=0 rtw_enusbss=0" | sudo tee /etc/modprobe.d/8192eu.conf


cd -
echo -e "\nPWD: $(pwd)"
fi
