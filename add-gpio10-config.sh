#!/bin/bash

#Adding GPIO10 config to rc.local

cat /etc/rc.local | grep '/sys/class/gpio/gpio10/direction'

if [ $? -eq 0 ]; then

	echo -e "\n\tgpio10 already configured in rc.local.\n"
else
	echo -e "\n\tAdding gpio10 configuration to rc.local. . .\n"
	
	sudo cat /etc/rc.local | sed '/done/a\\necho 10 \> \/sys\/class\/gpio\/export\n' | sed '/echo 10/a\echo out \> /sys/class/gpio/gpio10/direction\n' > /etc/rc.local.tmp
	sudo mv -f -b /etc/rc.local.tmp /etc/rc.local
	sudo chmod a+x /etc/rc.local
	
	echo -e "\n\tDONE\n"
fi