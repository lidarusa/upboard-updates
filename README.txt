
Version 1.0

Temporary update scripts to bring systems to latest version. Includes both system and webapp updates

System side:
WiFi fix (re-config if hostapd does not come up after boot)
Drive fix (detect drive, prevent mounting of local eMMC as /media/usb)

WebApp:
Include INS Scripts (snoopy2-il, etc)
Set sony-a6-trigger permissions. (chmod a+x)

