#!/bin/bash

echo -ne "Copying cpufreq.sh...\n"

cp /media/usb/upboard-updates/cpufreq.sh /home/lumberjack/
sleep 1

echo -ne "chmod a+x cpufreq.sh...\n"
chmod a+x /home/lumberjack/cpufreq.sh

exit 0
