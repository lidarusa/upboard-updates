#!/bin/bash

echo -e "\n\tCopying docker-timeout. . .\n"
sudo cp /media/usb/upboard-updates/docker-timeout.sh /home/lumberjack/

echo -e "\n\tSetting a+x check-hostapd permissions. . .\n"
sudo chmod a+x /home/lumberjack/docker-timeout.sh

echo -e "\n\tChecking rc.local for check-hostapd. . .\n"
cat /etc/rc.local | grep docker-timeout.sh

if [ $? -eq 0 ]; then

	echo -e "\n\tdocker-timeout.sh already executed in rc.local.\n"
else
	echo -e "\n\tAdding docker-timeout.sh to rc.local. . .\n"
	
	sudo sed -i 's/exit 0/\/home\/lumberjack\/docker-timeout.sh \&/' /etc/rc.local
	echo -e "\nexit 0" >> /etc/rc.local
	
	echo -e "\n\tDONE\n"
fi

sudo chmod a+x /etc/rc.local
