#!/bin/bash

echo -e "\n\tUpdating webapp using: firmware-update-*.tar"
docker load --input /media/usb/upboard-updates/firmware-update-*.tar

sleep 2

echo -e "\n\tdocker-compose down and up. . ."

cd ~/compose-snoopy-webapp
docker-compose down && docker-compose up -d

sleep 2
echo -e "\n\tRemoving unused images. . ."

docker image prune -f

echo -e "\n\tDONE.\n"

