#!/bin/bash

#Apply ALL updates

echo -e "\n\e[1m\e[32mStarting update handler...\n\e[0m"

echo -e "\n\e[1m\e[32mDisable SysRq. . .\n\e[0m"
	sudo /media/usb/upboard-updates/disable-sysrq.sh

echo -e "\n\e[1m\e[32mDisable ttyS1.conf. . .\n\e[0m"
	sudo /media/usb/upboard-updates/disable-ttyS1-getty.sh 

echo -e "\n\e[1m\e[32mDisable Grub Serial. . .\n\e[0m"
	sudo /media/usb/upboard-updates/disable-grub-serial.sh

echo -e "\n\e[0m\e[32mRunning install-check-hostapd.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/install-check-hostapd.sh
	sleep 1
	
echo -e "\n\e[0m\e[32mRunning install-docker-timeout.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/install-docker-timeout.sh
	sleep 1
	
echo -e "\e[32mRunning add-gpio10-config.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/add-gpio10-config.sh

echo -e "\e[32mRunning update-udev-rules.sh...\n\e[0m"
	sudo /media/usb/upboard-updates/update-udev-rules.sh

echo -e "\e[32mRunning update-mount-disk.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/update-mount-disk.sh
	
echo -e "\e[32mRunning update-docker-conf.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/update-docker-conf.sh
	
echo -e "\e[32mRunning add-compose-settings.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/add-compose-settings.sh

echo -e "\e[32mRunning disable-maps-container.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/disable-maps-container.sh 
	sleep 1

echo -e "\e[32mRunning update-webapp.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/update-webapp.sh
	sleep 1

echo -e "\n\e[32mRunning update-hcd.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/update-hcd.sh
	sleep 1

echo -e "\n\e[32mRestart webapp container. . .\n\e[0m"
	docker restart webapp
	sleep 5

echo -e "\n\e[32mRunning chmod-sony-a6.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/chmod-sony-a6.sh
	
echo -e "\n\e[32mInstall TP-WN823EU (rtl8192eu) Driver. . .\n\e[0m"
	sudo /media/usb/upboard-updates/install-rtl8192eu.sh

echo -e "\e[32mRunning up-gap-fix.sh...\n\e[0m"
	sudo /media/usb/upboard-updates/up-gap-fix.sh
	
#echo -e "\n\e[32mRunning install-srec.sh. . .\n\e[0m"
        #sudo /media/usb/upboard-updates/install-srec.sh

#echo -e "\n\e[32mRunning side-load-updates.sh. . .\n\e[0m"
#	sudo /media/usb/upboard-updates/side-load-updates.sh

if [ "$1" = "m8-update" ]; then
	echo -e "\n\e[32mRunning m8-update.sh. . .\n\e[0m"
	sudo /media/usb/upboard-updates/m8-update/m8-update.sh
fi

echo -e "\n\n\e[32m\e[1mALL UPDATES APPLIED!!!! Please remember to:\n\e[0m"
echo -e "\n\n\e[32m\e[1m\t1)Double check device config (insDeviceIndex, lidarDeviceIndex, cameraDeviceIndex, productIndex)\n\e[0m"
echo -e "\n\n\e[32m\e[1m\t2)Depending on device, double check /etc/udev/rules.d/persistent-usb.rules (drive labels, etc)\n\e[0m"
echo -e "\n\n\e[32m\e[1m\t3)Reboot, then bench test system\n\e[0m"
