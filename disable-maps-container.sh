#!/bin/bash

echo -e "\n\t\e[1m\e[31m Disabling MAPS container. . .\e[0m"
cd ~/compose-snoopy-webapp
docker-compose down --remove-orphans
sleep 1
sudo cp /media/usb/upboard-updates/docker-compose.yml ~/compose-snoopy-webapp/
docker-compose up -d
cd -
echo -e "\n\tDONE\n"

