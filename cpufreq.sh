#!/bin/bash

#echo 0 > /sys/devices/system/cpu/intel_pstate/no_turbo
echo "cpufreq-set.sh: intel_pstate.no_turbo: $(cat /sys/devices/system/cpu/intel_pstate/no_turbo)" > /dev/kmsg

echo "cpufreq-set.sh: SLEEP 60..." > /dev/kmsg

sleep 60

for i in {0..3}
do
  echo -ne "cpufreq-set.sh: Current setting ($i): $(cat /sys/devices/system/cpu/cpufreq/policy$i/scaling_governor)\n"
  echo performance > /sys/devices/system/cpu/cpufreq/policy$i/scaling_governor
  echo -ne "cpufreq-set.sh: NEW setting ($i): $(cat /sys/devices/system/cpu/cpufreq/policy$i/scaling_governor)\n\n"
  echo "cpufreq-set.sh: CPU$i scaling_governor: $(cat /sys/devices/system/cpu/cpufreq/policy$i/scaling_governor)" > /dev/kmsg
done
#service thermald stop
#echo -e "thermald: $(service thermald status)" >/dev/kmsg
#echo -e "cpufreq-set.sh: PRE-modprobe: thermal/cooling_device state (current, max): ($(cat /sys/class/thermal/cooling_device*/cur_state), $(cat /sys/class/thermal/cooling_device*/max_state))" > /dev/kmsg
echo "cpufreq-set.sh: REMOVING intel_* CPU drivers = FALSE" > /dev/kmsg
#modprobe -r intel_idle
#modprobe -r intel_powerclamp
#modprobe -r intel_rapl

#echo -e "cpufreq-set.sh: POST-modprobe: thermal/cooling_device state (current, max): ($(cat /sys/class/thermal/cooling_device*/cur_state), $(cat /sys/class/thermal/cooling_device*/max_state))" > /dev/kmsg
