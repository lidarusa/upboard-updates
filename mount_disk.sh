#!/bin/bash

if [ $1 == "ufsd" ]; then
  /usr/local/bin/chkntfs /dev/$3 -f
fi

if [ $1 == "ext4" ]; then
  fsck -y /dev/$3
fi

if [ $1 == "auto" ]; then
  fsck -y /dev/$3
fi

if [ $1 == "vfat" ]; then
  fsck -y /dev/$3
fi

/bin/mkdir -p /media/usb

/bin/mount -t $1 -o $2 /dev/$3 /media/usb

/sbin/initctl emit usbmounted MOUNTPOINT=/media/usb

if [ $1 == "ext4" ]; then
    #chown -R lumberjack:lumberjack /media/usb
fi

/usr/bin/docker restart webapp &

exit 0
