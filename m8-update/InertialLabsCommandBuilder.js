"use strict";

var moment = require('moment');
var fs = require('fs');
var path = require('path');
var config = require('./config/local.env');

var commands = {
  control: {
    INS_SensorsData: 0x50,
    INS_FullData: 0x51,
    INS_OPVTdata: 0x52,
    INS_OPVT2AHRdata: 0x58,
    INS_QPVTdata: 0x56,
    INS_minData: 0x53,
    INS_NMEA: 0x54,
    INS_Sensors_NMEA: 0x55,
    INS_TimedIMU: 0x62,
    INS_TSS1: 0x35,
    SetOnRequestMode: 0xC1,
    Stop: 0xFE,
    LoadINSpar: 0x40,
    ReadINSpar: 0x41,
    GetDevInfo: 0x12,
    GetBIT: 0x1A
  },
  calibration: {
    Start2DClb: 0x21,
    Start2D2TClb: 0x22,
    Start3DClb: 0x23,
    StartClbRun: 0x2B,
    StopClbRun: 0x20,
    FinishClb: 0x2C,
    AcceptClb: 0x2E,
    ExitClb: 0xFE,
    ClearClb: 0x2F,
    GetClbRes: 0x2A
  },
  flash: {
    ACK: 0x0,
    ReadFlash: 0x0B,
    WriteFlash: 0x0E,
    ProgramFlash: 0xFF
  },
  syncLength: 2,
  headerLength: 6,
  readINSparLength: 60,
  checksumLength: 2
};

function backupFlash(chunk) {
  var flashBackup = path.join(config.licenseRoot, 'flash-backup-' + moment().format('MM-DD-YYYY-hh-mm-ss') + '.bin');
  var flashBackupStream = fs.createWriteStream(flashBackup);

  flashBackupStream.write(chunk);
  flashBackupStream.close();
}

function checksum16(chunk) {
  var sum = 0;
  for(var i = 0; i < chunk.length; i++) {
    sum += chunk[i];
  }

  return sum & 0xFFFF;
}

function rawLoadIns(chunk, dataRate, insSettings) {
  var payload = chunk.slice(commands.headerLength);
  var avgNum = (dataRate === 200) ? 1 : 2;

  // write data rate
  payload.writeUInt16LE(dataRate, 6);
  payload.writeUInt16LE(avgNum, 8);
  //write dataRate
  payload.writeUInt16LE(dataRate, 18);
  //new FW dataRate divisor
  
  // write orientation
  payload.writeFloatLE(insSettings.orientation.z, 570);
  payload.writeFloatLE(insSettings.orientation.x, 574);
  payload.writeFloatLE(insSettings.orientation.y, 578);

  // default all INSBs to 20Hz GNSS COM2 output
  payload.writeUInt8(6, 939);
  
  //M8 PPS pulsewidth 
  payload.writeUInt8(0, 1537);
  payload.writeUInt16LE(50000, 1539);
  
  // write antenna lever arms
  var ANTENNA_SCALE = 100.0;
  payload.writeInt16LE(Math.floor(insSettings.antenna1.x * ANTENNA_SCALE), 1570);
  payload.writeInt16LE(Math.floor(insSettings.antenna1.y * ANTENNA_SCALE), 1572);
  payload.writeInt16LE(Math.floor(insSettings.antenna1.z * ANTENNA_SCALE), 1574);

  // write CoG lever arms
  payload.writeInt16LE(0, 1576);
  payload.writeInt16LE(0, 1578);
  payload.writeInt16LE(0, 1580);

  return chunk;
}

function printFloatMatrix(chunk, offset, rows, cols) {
  var matrixStr = "[\n";
  for(var i = 0; i < rows; i++) {
    matrixStr += "[ ";
    for(var j = 0; j < cols; j++) {
      matrixStr += chunk.readFloatLE(offset + 4*i*cols + 4*j) + " ";
    }
    matrixStr += "],\n";
  }
  matrixStr += "]\n";

  console.log(matrixStr);
}

function switchToInternalAccelerometer(chunk) {
  var payload = chunk.slice(6, 2054);
  payload.writeUInt8(0, 948);

  // diag1
  payload.writeFloatLE(9.8106, 454); payload.writeFloatLE(9.8106, 458); payload.writeFloatLE(9.8106, 462);

  // cross
  payload.writeFloatLE(1.0, 106); payload.writeFloatLE(0.0, 110);  payload.writeFloatLE(0.0, 114);
  payload.writeFloatLE(0.0, 118); payload.writeFloatLE(1.0, 122);  payload.writeFloatLE(0.0, 126);
  payload.writeFloatLE(0.0, 130); payload.writeFloatLE(0.0, 134);  payload.writeFloatLE(1.0, 138);

  // bias
  payload.writeFloatLE(0.0, 142); payload.writeFloatLE(0.0, 146);  payload.writeFloatLE(0.0, 150);

  // temp_bias_line_step
  payload.writeFloatLE(0.0, 358); payload.writeFloatLE(0.0, 362);  payload.writeFloatLE(0.0, 366);
  payload.writeFloatLE(0.0, 370); payload.writeFloatLE(0.0, 374);  payload.writeFloatLE(0.0, 378);

  // temp_scale_line_step
  payload.writeFloatLE(0.0, 382); payload.writeFloatLE(0.0, 386);  payload.writeFloatLE(0.0, 390);
  payload.writeFloatLE(0.0, 394); payload.writeFloatLE(0.0, 398);  payload.writeFloatLE(0.0, 402);

  // temp_bias_poly
  payload.writeFloatLE(0.0, 793); payload.writeFloatLE(0.0, 797);  payload.writeFloatLE(0.0, 801);
  payload.writeFloatLE(0.0, 805); payload.writeFloatLE(0.0, 809);  payload.writeFloatLE(0.0, 813);

  // temp_scale_poly
  payload.writeFloatLE(0.0, 817); payload.writeFloatLE(0.0, 821);  payload.writeFloatLE(0.0, 825);
  payload.writeFloatLE(0.0, 829); payload.writeFloatLE(0.0, 833);  payload.writeFloatLE(0.0, 837);

  // apply gyro fix if necessary
  payload.writeFloatLE(Math.PI / 180.0, 470);

  return chunk;
}

function readLatestFirmware() {
  var files = fs.readdirSync(config.licenseRoot);

  var latestBackup = files
    .filter(function(file) {
      return file.match(/flash-backup.*/);
    })
    .reduce(function(prev, curr) {
      var re = /flash-backup-(.*)\.bin/;
      var prevDate = moment(re.exec(prev)[1], 'MM-DD-YYYY-hh-mm-ss');
      var currDate = moment(re.exec(curr)[1], 'MM-DD-YYYY-hh-mm-ss');

      return currDate.isAfter(prevDate) ? curr : prev;
    });

  return fs.readFileSync(path.join(config.licenseRoot, latestBackup));
}

module.exports = {
  commands: commands,
  build: function(command) {
    var baseCommand = new Buffer([0xAA, 0x55, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00]);
    baseCommand.writeUInt8(command, 6);
    baseCommand.writeUInt16LE(checksum16(baseCommand.slice(2, -2)), 7);

    if(command === 0xFF) {
      baseCommand = new Buffer([0xAA, 0x55, 0x00, 0x00, 0x0B, 0x00, 0xFF, 0xEA, 0x5E, 0xDE, 0xAD, 0xDD, 0x03]);
    }

    return baseCommand;
  },
  checksum16: checksum16,
  rawLoadIns: rawLoadIns,
  backupFlash: backupFlash,
  readLatestFirmware: readLatestFirmware,
  switchToInternalAccelerometer: switchToInternalAccelerometer
};
