#!/bin/bash

docker exec -it webapp cp /usr/src/app/server/InertialLabsCommandBuilder.js{,.BAK}
docker exec -it webapp cp /usr/src/app/server/InertialLabsInsLogger.js{,.BAK}

docker cp /media/usb/upboard-updates/m8-update/InertialLabsCommandBuilder.js webapp:/usr/src/app/server/
docker cp /media/usb/upboard-updates/m8-update/InertialLabsInsLogger.js webapp:/usr/src/app/server/

cp /etc/rc.local{,.BAK}
cp /media/usb/upboard-updates/m8-update/rc.local /etc/rc.local
echo -ne $(date) >> /home/lumberjack/m8-update.txt

