"use strict";

var BaseInsLogger = require('./BaseInsLogger');
var inherits = require('util').inherits;
var fs = require('fs');
var il = require('./InertialLabsCommandBuilder');
var imrutils = require('./imr-utils');
var Int64 = require('node-int64');

var SerialPort = require('serialport');
var spy = require('through2-spy');
var map = require('through2-map');
var sink = require('through2-sink');
var filter = require('through2-filter');
var ilSplit = require('./through2-il-split');
var delayedSpy = require('./through2-delayed-spy');
var settingsManager = require('./settings-manager');

var CoverageMapController = require('./CoverageMapController');
//var systemLogger = require('./system-logger');

function InertialLabsInsLogger(insDevice, logFile, udpServer) {
  BaseInsLogger.call(this, insDevice, logFile);

  var self = this;

  self.START = il.build(il.commands.control.INS_OPVT2AHRdata);
  self.STOP = il.build(il.commands.control.Stop);
  self.imuFilename = self.logFile + '-Snoopy.imr';
  self.imuStream = fs.createWriteStream(self.imuFilename);

  self.topFilename = self.logFile + '.top';
  self.topStream = fs.createWriteStream(self.topFilename);
  self.isAligned = false;
  self.hasGps = false;

  self.insStatusTime = process.hrtime();
  self.lineMetricsTime = process.hrtime();

  self.serialStreamImu = new SerialPort(self.insDevice.imu.device, {
    baudRate: self.insDevice.imu.baudRate,
    autoOpen: false
  });

  self.topStream.write('TOP_VERSION: 1.2');

  self.serialStreamImu.on('error', function(err) {
    self.closeSerialStreamIn();
    self.emit('error', 'Failed to open ' + self.insDevice.imu.device + ' for IMU logging.');
  });

  self.coverageMap = new CoverageMapController(self, logFile, udpServer);
}
inherits(InertialLabsInsLogger, BaseInsLogger);

InertialLabsInsLogger.prototype.onStart = function() {
  var self = this;

  self.serialStreamImu.open(function() {
    self._startStreams();
  });
};

InertialLabsInsLogger.prototype._startStreams = function() {
  var self = this;

  var startedSpy = spy(function(chunk) {
    self.notifyLoggingStarted(chunk);
  });

  var devInfoSpy = spy(function(chunk) {
    var messageType = chunk.readUInt8(3);
    if(messageType === il.commands.control.GetDevInfo) {
      var payload = chunk.slice(6); // strip header
      var firmwareId = payload.slice(8, 48).toString();
//comment
      if(firmwareId.toLowerCase().indexOf('imu2k') > -1) {
        self.imuStream.write(
          imrutils.createImrHeader({
            name: 'Inertial Labs INS B/C',
            dataRate: self.insDevice.dataRate,
            gyroScale: 1.0e-5,
            accelScale: 9.8106 / 1.0e6
          })
        );
      } else {
        self.imuStream.write(
          imrutils.createImrHeader({
            name: 'Inertial Labs INS B/C',
            dataRate: self.insDevice.dataRate,
            gyroScale: 1.0e-5,
            accelScale: 9.8106 / 1.0e6
          })
        );
      }
    }
  });

  var alignmentStatusFilter = filter(function(chunk) {
    var packetSize = chunk.readUInt16LE(4);
    if(packetSize === 0x38) {
      var status = chunk.readUInt16LE(54);
      if(status === 0) {
        self.isAligned = true;
      }

      self.emit('ins:alignmentCompleted', status);
    }

    return self.isAligned;
  });

  var isAlignedFilter = filter(function(chunk) {
    return self.isAligned;
  });

  var opvt2ahrFilter = filter(function(chunk) {
    var messageType = chunk.readUInt8(3);
    var packetSize = chunk.readUInt16LE(4);
    return messageType === il.commands.control.INS_OPVT2AHRdata && packetSize === 135;
  });

  var gpsFilter = filter(function(chunk) {
    var self = this;
    var payload = chunk.slice(6); // strip header
    var gnssInfo2 = payload.readUInt8(109);
    var solutionStatus = gnssInfo2 & 0x03;

    if(solutionStatus === 0) {
      self.hasGps = true;
    }

    return self.hasGps;
  });

  var checksumFilter = filter(function(chunk) {
    var expectedChecksum = il.checksum16(chunk.slice(2, -2));
    var actualChecksum = chunk.readUInt16LE(chunk.length - 2);

    return expectedChecksum === actualChecksum;
  });

  var statusSink = sink(function(chunk) {
    var diff = process.hrtime(self.insStatusTime);
    var payload = chunk.slice(6); // strip header

    var h = payload.readUInt16LE(0);
    var lat = new Int64(new Buffer([
        payload[49], payload[48],
        payload[47], payload[46],
        payload[45], payload[44],
        payload[43], payload[42]
      ])
    );

    var lon = new Int64(new Buffer([
        payload[57], payload[56],
        payload[55], payload[54],
        payload[53], payload[52],
        payload[51], payload[50]
      ])
    );

    self.coverageMap.updatePose({
      lat: lat * 1e-9,
      lon: lon * 1e-9,
      bearing: h * 1e-2
    });

    if(diff[0] > 0) {
      self.insStatusTime = process.hrtime();

      var hrInsStatus = self._createInsStatus(payload);

      self.emit(self.INS_STATUS_EVENT, hrInsStatus);
    }
  });

  var topMap = map(function(chunk) {
    var topData = new Buffer(81);
    var payload = chunk.slice(6);

    var gpsTow = payload.readUInt32LE(104);

    var h = payload.readUInt16LE(0);
    var p = payload.readInt16LE(2);
    var r = payload.readInt16LE(4);

    var lat = new Int64(new Buffer([
        payload[49], payload[48],
        payload[47], payload[46],
        payload[45], payload[44],
        payload[43], payload[42]
      ])
    );

    var lon = new Int64(new Buffer([
        payload[57], payload[56],
        payload[55], payload[54],
        payload[53], payload[52],
        payload[51], payload[50]
      ])
    );

    var alt = payload.readInt32LE(58);

    var latGnss = new Int64(new Buffer([
        payload[81], payload[80],
        payload[79], payload[78],
        payload[77], payload[76],
        payload[75], payload[74]
      ])
    );

    var lonGnss = new Int64(new Buffer([
        payload[89], payload[88],
        payload[87], payload[86],
        payload[85], payload[84],
        payload[83], payload[82]
      ])
    );

    var altGnss = payload.readInt32LE(90);
    var gnssInfo1 = payload.readUInt8(108);

    var offsetWrite = 0;
    topData.writeDoubleLE(gpsTow * 1e-3, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(h * 1e-2, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(p * 1e-2, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(r * 1e-2, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(lat * 1e-9, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(lon * 1e-9, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(alt * 1e-3, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(latGnss * 1e-9, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(lonGnss * 1e-9, offsetWrite); offsetWrite += 8;
    topData.writeDoubleLE(altGnss * 1e-3, offsetWrite); offsetWrite += 8;
    topData.writeUInt8(gnssInfo1, offsetWrite);

    return topData;
  });

  var imrMap = map(function(chunk, enc, cb) {
    var imrData = new Buffer(32);
    var payload = chunk.slice(6); // strip header

    var offsetWrite = 0;
    var gpsTime = payload.readUInt32LE(104);
    imrData.writeDoubleLE((gpsTime * 1e-3), offsetWrite); offsetWrite += 8;

    var gx = payload.readInt32LE(6);
    var gy = payload.readInt32LE(10);
    var gz = payload.readInt32LE(14);

    var ax = payload.readInt32LE(18);
    var ay = payload.readInt32LE(22);
    var az = payload.readInt32LE(26);

    imrData.writeInt32LE(gx, offsetWrite); offsetWrite += 4;
    imrData.writeInt32LE(gy, offsetWrite); offsetWrite += 4;
    imrData.writeInt32LE(gz, offsetWrite); offsetWrite += 4;

    imrData.writeInt32LE(ax, offsetWrite); offsetWrite += 4;
    imrData.writeInt32LE(ay, offsetWrite); offsetWrite += 4;
    imrData.writeInt32LE(az, offsetWrite);

    return imrData;
  });

  if(settingsManager.getCurrentLidarDevice().name === "Velodyne") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('log com3 gprmc ontime 1\r\n');
    self.serialStreamIn.write('log com3 gprmc ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Hesai" || settingsManager.getCurrentLidarDevice().name === "Surveyor") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('log com3 gprmc ontime 1\r\n');
    self.serialStreamIn.write('log com3 gprmc ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Riegl") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('log com3 gpgga ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Optech") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 38400 n 8 1 n off\r\n'); //serialconfig and omit last on per OG board; Set WAAS 1 for GGA to be in spec
    self.serialStreamIn.write('log com3 gpzda ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Z+F") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('log com3 gpzda ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Triple-In") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n'); //serialconfig and omit last on per OG board; Set WAAS 1 for GGA to be in spec
    self.serialStreamIn.write('log com3 gpzda ontime 1\r\n');
  }
  else if(settingsManager.getCurrentLidarDevice().name === "Quanergy") {
    self.serialStreamIn.write('unlogall com3\r\n');
    self.serialStreamIn.write('serialconfig com3 9600 n 8 1 n off\r\n');
    self.serialStreamIn.write('log com3 gprmc ontime 1\r\n');
  }


  self.serialStreamIn.write('unlogall thisport\r\n');

  self.serialStreamIn.write('log rangecmpb ontime 0.05\r\n');
  self.serialStreamIn.write('log rawephemb onnew\r\n');
  self.serialStreamIn.write('log gloephemerisb onnew\r\n');
  self.serialStreamIn.write('log bestposb ontime 1\r\n');
  self.serialStreamIn.write('log timeb ontime 1\r\n');
  self.serialStreamIn.write('markcontrol mark2 enable positive\r\n');
  self.serialStreamIn.write('log mark2timeb onnew\r\n');
//log twice for older INSBs
  self.serialStreamIn.write('log rangecmpb ontime 0.05\r\n');
  self.serialStreamIn.write('log rawephemb onnew\r\n');
  self.serialStreamIn.write('log gloephemerisb onnew\r\n');
  self.serialStreamIn.write('log bestposb ontime 1\r\n');
  self.serialStreamIn.write('log timeb ontime 1\r\n');
  self.serialStreamIn.write('markcontrol mark2 enable positive\r\n');
  self.serialStreamIn.write('log mark2timeb onnew\r\n');

  self.serialStreamIn
    .pipe(isAlignedFilter)
    .pipe(self.insStream);

  setTimeout(function(){
    self.serialStreamIn.write('log versionb once\r\n'); //log receiver version info for troubleshooting etc.
  }, 10000);

  self.serialStreamImu.write(self.STOP);

  setTimeout(function() {
    self.serialStreamImu.write(il.build(il.commands.control.GetDevInfo));

    self.serialStreamImu.write(self.START);
    self.emit('ins:alignmentStarted');

    var rootImuStream = self.serialStreamImu
      .pipe(ilSplit())
      .pipe(devInfoSpy)
      .pipe(checksumFilter)
      .pipe(alignmentStatusFilter)
      .pipe(opvt2ahrFilter)
      .pipe(gpsFilter)
      .pipe(startedSpy)
      .pipe(delayedSpy({delay: 5000.0}));

    rootImuStream
      .pipe(statusSink);

    rootImuStream
      .pipe(topMap)
      .pipe(self.topStream);

    rootImuStream
      .pipe(imrMap)
      .pipe(self.imuStream);
  }, 1000);
};

InertialLabsInsLogger.prototype.onPreStop = function(callback) {
  var self = this;

  self.coverageMap.close();

  if(self.serialStreamImu !== null) {
    self.serialStreamImu.write(self.STOP);
    self.serialStreamImu.unpipe();
    self.serialStreamImu.end();
    self.serialStreamImu.close();
  }

  if(self.imuStream !== null) {
    self.imuStream.close();
  }

  callback();
};

InertialLabsInsLogger.prototype._createInsStatus = function(payload) {
  var self = this;

  var usw = payload.readUInt16LE(37);
  var voltageInput = payload.readUInt16LE(38);
  var temp = payload.readInt16LE(40);
  var gnssInfo1 = payload.readUInt8(108);
  var gnssInfo2 = payload.readUInt8(109);
  var satCount = payload.readUInt8(110);


  var hrStatus = Object.assign(
    {},
    self._parseGnssInfo1(gnssInfo1),
    self._parseGnssInfo2(gnssInfo2)
  );
  hrStatus.satelliteCount = satCount;
  hrStatus.temperature = temp / 10.0 + ' Celsius';
  hrStatus.batteryVoltage = voltageInput / 100.0 + ' V';
  /*
  hrStatus.uswPowerLow = usw & 0x100; //bit 8
  hrStatus.uswPowerHigh = usw & 0x200; //bit 9
  hrStatus.uswTemp = usw & 0x4000; //bit 14
  */

  return hrStatus;
};

InertialLabsInsLogger.prototype._parseGnssInfo1 = function(gnssInfo1) {
  var gnssInfoObj1 = {};

  var positionText = [
    'Single point',
    'DGPS',
    'SBAS',
    'PPP',
    'RTK Other',
    'RTK Narrow-int',
    'Other'
  ];

  var correctionText = [
    'Unknown',
    'Klobuchar',
    'SBAS',
    'Multi-frequency',
    'DGPS',
    'Novatel blended'
  ];

  gnssInfoObj1.positionType = positionText[gnssInfo1 & 0x0F];
  gnssInfoObj1.correctionType = correctionText[(gnssInfo1 >> 4) & 0x0F];

  return gnssInfoObj1;
};

InertialLabsInsLogger.prototype._parseGnssInfo2 = function(gnssInfo2) {
  var gnssInfoObj2 = {};

  var solutionText = [
    'Computed',
    'Insufficient observations',
    'Converging',
    'No solution'
  ];

  var timeStatusText = [
    'Unknown',
    'Coarse',
    'Lost',
    'Fine'
  ];

  gnssInfoObj2.solutionStatus = solutionText[gnssInfo2 & 0x03];
  gnssInfoObj2.timeStatus = timeStatusText[(gnssInfo2 >> 2) & 0x03];
  gnssInfoObj2.gpsSignal = ((gnssInfo2 >> 4) & 0x01) ? 'True' : 'False';
  gnssInfoObj2.glonassSignal = ((gnssInfo2 >> 5) & 0x01) ? 'True' : 'False';
  gnssInfoObj2.galileoSignal = ((gnssInfo2 >> 6) & 0x01) ? 'True' : 'False';
  gnssInfoObj2.beidouSignal = ((gnssInfo2 >> 7) & 0x01) ? 'True' : 'False';

  return gnssInfoObj2;
};

module.exports = InertialLabsInsLogger;
